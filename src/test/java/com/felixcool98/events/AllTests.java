package com.felixcool98.events;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ConsumableEventTest.class,
	NormalEventTest.class,
	SingleObjectEventTest.class,
	TestGenericEvent.class})
public class AllTests {

}
