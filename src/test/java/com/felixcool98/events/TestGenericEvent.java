package com.felixcool98.events;

import static org.junit.Assert.*;

import org.junit.Test;

import com.felixcool98.events.fast.FastEventManager;

public class TestGenericEvent {

	@Test
	public void test() {
		FastEventManager manager = new FastEventManager();
		
		Receiver test = new Receiver();
		
		manager.listen(test);
		
		assertEquals(0, test.i);
		assertEquals(0, test.a);
		
		manager.send(new GenericInterfaceEvent<String>() {

			@Override
			public String get() {
				return "hello world";
			}
		});
		
		manager.send(new GenericAbstractEvent<String>() {
			@Override
			public String get() {
				return "hello world 2";
			}
		});
		
		assertEquals(0, test.i);
		assertEquals(0, test.a);
		
		manager.send(new GenericInterfaceEvent<Integer>() {

			@Override
			public Integer get() {
				return 2;
			}
		});
		
		manager.send(new GenericAbstractEvent<Integer>() {
			@Override
			public Integer get() {
				return 5;
			}
		});
		
		assertEquals(2, test.i);
		assertEquals(5, test.a);
		
		manager.send(new GenericInterfaceEventImpl());
		
		assertEquals(7, test.i);
		assertEquals(5, test.a);
		
		manager.send(new GenericInterfaceEventImpl() {
			@Override
			public Integer get() {
				return 1;
			}
		});
		
		assertEquals(8, test.i);
		assertEquals(5, test.a);
		
		manager.send(new WrappedGenericInterface<Integer>(new GenericWrappedInterfacePrep<Integer>() {
			@Override
			public Integer get() {
				return 1;
			}

			@Override
			public Class<Integer> getType() {
				return Integer.class;
			}
		}));
		
		assertEquals(9, test.i);
		assertEquals(5, test.a);
		
		//receiver  method(Event<T>)
		//sender  method(Event<T)>
	}

	public static interface GenericInterfaceEvent<T>{
		public T get();
	}
	public static class GenericInterfaceEventImpl implements GenericInterfaceEvent<Integer>{
		@Override
		public Integer get() {
			return 5;
		}
	}
	
	public static abstract class GenericAbstractEvent<T>{
		public abstract T get();
	}
	
	public static class Receiver{
		private int i = 0;
		private int a = 0;
		
		@EventListener
		public void genericInterface(WrappedGenericInterface<Integer> event) {
			i += event.get();
		}
		@EventListener
		public void genericInterface(GenericInterfaceEvent<Integer> event) {
			i += event.get();
		}
		@EventListener
		public void genericAbstract(GenericAbstractEvent<Integer> event) {
			a += event.get();
		}
	}
	
	public static interface GenericWrappedInterfacePrep<T> extends GenericInterfaceEvent<T>, GenericEvent<T>{
		@Override
		public Class<T> getType();
	}
	public static class WrappedGenericInterface<T> implements GenericEvent<T>{
		private GenericWrappedInterfacePrep<T> g;

		
		public WrappedGenericInterface(GenericWrappedInterfacePrep<T> g) {
			this.g = g;
		}
		
		
		public T get() {
			return g.get();
		}
		
		@Override
		public Class<T> getType() {
			return g.getType();
		}
	}
}
