package com.felixcool98.events;

import com.felixcool98.events.fast.FastEventManager;

public class EventFieldTest {
	public static void main(String[] args) {
		FastEventManager manager = new FastEventManager();
		
		manager.listen(new TestClass());
		
		manager.send(new TestEvent("1"));
		manager.send(new TestEvent("2"));
		
		System.out.println("-----");
		
		manager.listen(new TestClass());
		
		manager.send(new TestEvent("3"));
		manager.send(new TestEvent("4"));
	}
	
	
	private static class TestClass {
		@EventListener(fieldNames = {"name"})
		public void testMethod(TestEvent event, String name) {
			System.out.println("testMethod "+name);
		}
	}
	private static class TestEvent {
		@SuppressWarnings("unused")
		private String name;
		
		
		private TestEvent(String name) {
			this.name = name;
		}
	}
}
