package com.felixcool98.events;

import static org.junit.Assert.*;

import org.junit.Test;

import com.felixcool98.events.fast.FastEventManager;

public class SingleObjectEventTest {
	int i = 0;
	
	@Test
	public void test() {
		FastEventManager manager = new FastEventManager();
		
		manager.sendTo(new TestClass(), new TestEvent());
		
		assertEquals(1, i);
		
		manager.send(new TestEvent());
		
		assertEquals(1, i);
		
		manager.listen(new TestClass());
		
		manager.sendTo(new TestClass(), new TestEvent());
		
		assertEquals(2, i);
		
		manager.send(new TestEvent());
		
		assertEquals(3, i);
	}
	
	
	public class TestEvent{
		
	}
	
	public class TestClass{
		@EventListener
		public void emit(TestEvent event) {
			i++;
		}
	}
}
