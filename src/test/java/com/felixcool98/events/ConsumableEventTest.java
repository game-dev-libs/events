package com.felixcool98.events;

import static org.junit.Assert.*;

import org.junit.Test;

import com.felixcool98.events.ConsumableEvent.ConsumeableEventAdapter;
import com.felixcool98.events.fast.FastEventManager;

public class ConsumableEventTest {
	private int i = 0;
	

	@Test
	public void test() {
		FastEventManager manager = new FastEventManager();
		
		manager.listen(new TestClass());
		manager.listen(new TestClass());
		
		manager.send(new TestEvent());
		
		assertEquals(1, i);
	}

	
	public class TestEvent extends ConsumeableEventAdapter{
		
	}
	
	public class TestClass{
		@EventListener
		public void emit(TestEvent event) {
			i++;
			
			event.consume();
		}
	}
}
