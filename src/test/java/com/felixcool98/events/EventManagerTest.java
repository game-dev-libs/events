package com.felixcool98.events;

import com.felixcool98.events.fast.FastEventManager;

public class EventManagerTest {
	public static void main(String[] args) {
		FastEventManager manager = new FastEventManager();
		
		manager.listen(new TestClass());
		
		manager.send(new TestEvent());
		manager.send(new TestEvent());
		
		System.out.println("-----");
		
		manager.listen(new TestClass());
		
		manager.send(new TestEvent());
		manager.send(new TestEvent());
	}
	
	
	private static class TestClass {
		@EventListener
		public void testMethod(TestEvent event) {
			System.out.println("testMethod");
		}
		@EventListener
		public void testMethod2(Object event) {
			System.out.println("testMethod2");
		}
	}
	private static class TestEvent {
		
	}
}
