package com.felixcool98.events;

import static org.junit.Assert.*;

import org.junit.Test;

import com.felixcool98.events.fast.FastEventManager;

public class NormalEventTest {
	private int i = 0;
	

	@Test
	public void test() {
		FastEventManager manager = new FastEventManager();
		
		manager.listen(new TestClass());
		manager.listen(new TestClass());
		
		manager.send(new TestEvent());
		
		assertEquals(2, i);
		
		TestClass test = new TestClass();
		
		manager.listen(test);
		
		manager.send(new TestEvent());
		
		assertEquals(5, i);
		
		manager.stopListening(test);
		
		manager.send(new TestEvent());
		
		assertEquals(7, i);
	}

	
	public class TestEvent{
		
	}
	
	public class TestClass{
		@EventListener
		public void emit(TestEvent event) {
			i++;
		}
	}
}
