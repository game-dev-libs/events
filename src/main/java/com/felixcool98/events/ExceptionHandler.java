package com.felixcool98.events;

public interface ExceptionHandler {
	public void handle(Exception exception);
}
