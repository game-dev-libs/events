package com.felixcool98.events;

/**
 * interface used to make an event consumable
 * 
 * @author felixcool98
 */
public interface ConsumableEvent {
	/**
	 * consumes the event <br>
	 * no more listeners will receive the event
	 */
	public void consume();
	/**
	 * @return true if the event is consumed and should not be given to any listeners
	 */
	public boolean isConsumed();
	
	
	public static class ConsumeableEventAdapter implements ConsumableEvent {
		private boolean consumed;
		
		
		@Override
		public void consume() {
			consumed = true;
		}
		
		
		@Override
		public boolean isConsumed() {
			return consumed;
		}
	}
}
