package com.felixcool98.events.fast;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import com.felixcool98.events.EventManager;
import com.felixcool98.events.ExceptionHandler;
import com.felixcool98.events.GenericEvent;

public class FastEventManager implements EventManager {
	private Map<Class<?>, ClassWrapper> wrappers = new HashMap<Class<?>, ClassWrapper>();
	private Storage<ClassWrapper.WrappedMethod> methods = new Storage<ClassWrapper.WrappedMethod>();
	private HashSet<ClassWrapper.WrappedMethod> methodSet = new HashSet<ClassWrapper.WrappedMethod>();
	
	private boolean autoclean = true;
	
	private ExceptionHandler handler = (e)->{e.printStackTrace();};
	 
	
	@Override
	public void listen(Object listener) {
		if(!wrappers.containsKey(listener.getClass())) {
			ClassWrapper wrapper = new ClassWrapper(listener.getClass());
			
			wrappers.put(listener.getClass(), wrapper);
			
			methodSet.addAll(wrapper.getMethods());
		}
		
		wrappers.get(listener.getClass()).add(listener);
	}
	
	@Override
	public void stopListening(Object listener) {
		if(!wrappers.containsKey(listener.getClass()))
			return;
		
		ClassWrapper wrapper = wrappers.get(listener.getClass());
		
		wrapper.remove(listener);
		
		if(!autoclean || !wrapper.isEmpty())
			return;
		
		remove(wrapper);
	}
	
	@Override
	public void send(Object event) {
		for(ClassWrapper.WrappedMethod method : methodSet) {
			if(methods.contains(eventHashCode(event),method))
				continue;
			
			if(method.accepts(event))
				methods.add(eventHashCode(event), method);
		}
		
		for(ClassWrapper.WrappedMethod method : methods.get(eventHashCode(event))) {
			if(method.accepts(event)) {
				try {
					method.emit(event);
				} catch (Exception e) {
					if(handler != null)
						handler.handle(e);
				}
			}
		}
	}
	@Override
	public void sendTo(Object listener, Object event) {
		ClassWrapper wrapper;
		
		if(wrappers.containsKey(listener.getClass())) 
			wrapper = wrappers.get(listener.getClass());
		else
			wrapper = new ClassWrapper(listener.getClass());
		
		for(ClassWrapper.WrappedMethod method : wrapper.getMethods()) {
			if(method.accepts(event)) {
				try {
					method.emit(listener, event);
				} catch (Exception e) {
					if(handler != null)
						handler.handle(e);
				}
			}
		}
	}
	
	
	public void clean() {
		List<ClassWrapper> remove = new LinkedList<ClassWrapper>();
		
		for(ClassWrapper wrapper : wrappers.values()) {
			if(wrapper.isEmpty())
				remove.add(wrapper);
		}
		
		for(ClassWrapper wrapper : remove) {
			remove(wrapper);
		}
	}
	
	private void remove(ClassWrapper wrapper) {
		wrappers.remove(wrapper.getClass());
		
		for(ClassWrapper.WrappedMethod method : wrapper.getMethods()) {
			methods.remove(method);
		}
		
		methodSet.removeAll(wrapper.getMethods());
	}
	
	
	public void enableAutoClean() {
		autoclean = true;
	}
	public void disableAutoClean() {
		autoclean = false;
	}
	
	
	private int eventHashCode(Object event) {
		int hash = 0;
		
		hash += event.getClass().hashCode();
		
		if(event instanceof GenericEvent)
			hash += ((GenericEvent<?>) event).getType().hashCode()*7927;//prime
		
		return hash;
	}
	
	
	@Override
	public void setExceptionHandler(ExceptionHandler handler) {
		this.handler = handler;
	}
}
