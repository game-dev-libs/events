package com.felixcool98.events.fast;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class NormalMethod implements IMethod{
	private Method method;
	
	private String[] fieldNames;
	
	
	public NormalMethod(Method method, String[] fieldNames) {
		this.method = method;
		
		this.fieldNames = fieldNames;
	}
	
	
	@Override
	public boolean accepts(Object event) {
		return method.getParameterTypes()[0].isAssignableFrom(event.getClass());
	}

	@Override
	public Object invoke(Object object, Object event) throws Exception {
		Object[] parameters = new Object[method.getParameterCount()];
		
		parameters[0] = event;
		
		for(int i = 1; i < method.getParameters().length; i++) {
			Field field = event.getClass().getDeclaredField(fieldNames[i-1]);
			field.setAccessible(true);
			parameters[i] = field.get(event);
		}
		
		return method.invoke(object, parameters);
	}
}