package com.felixcool98.events.fast;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.LinkedList;
import java.util.List;

import com.felixcool98.events.ConsumableEvent;
import com.felixcool98.events.EventListener;

class ClassWrapper {
	private Class<?> classs;
	
	private List<Object> objects = new LinkedList<Object>();
	
	private List<WrappedMethod> methods = new LinkedList<ClassWrapper.WrappedMethod>();
	
	
	public ClassWrapper(Class<?> classs) {
		this.classs = classs;
		
		for(Method method : classs.getMethods()) {
			if(!method.isAnnotationPresent(EventListener.class))
				continue;
			
			method.setAccessible(true);
			
			EventListener listener = method.getAnnotation(EventListener.class);
			
			if(method.getParameterCount() != 1) {
				if(method.getParameterCount() != listener.fieldNames().length+1)
					throw new IllegalArgumentException(method+"\n"
							+ "has either the wrong number of parameters or the wrong number of fieldnames");
				
				Parameter eventType = method.getParameters()[0];
				
				for(int i = 1; i < method.getParameters().length; i++) {
					Parameter parameter = method.getParameters()[i];
					
					try {
						Field field = eventType.getType().getDeclaredField(listener.fieldNames()[i-1]);
						
						if(!parameter.getType().isAssignableFrom(field.getType())) 
							throw new IllegalArgumentException("Parameter "+i+" of "+method.getName()+" in "+classs.getTypeName()+" has wrong type, "
									+ "needs "+field.getType().getTypeName());
					} catch (NoSuchFieldException | SecurityException e) {
						throw new IllegalArgumentException("couldn't access "+listener.fieldNames()[i-1]+" in "+eventType.getType());
					}	
				}
			}
			
			this.methods.add(new WrappedMethod(method));
		}
	}
	
	
	public void add(Object object) {
		if(!object.getClass().equals(classs))
			throw new IllegalArgumentException("Object class != ClassWrapper classs");
		
		objects.add(object);
	}
	public void remove(Object object) {
		objects.remove(object);
	}
	
	
	public boolean isEmpty() {
		return objects.isEmpty();
	}
	
	
	public List<WrappedMethod> getMethods() {
		return methods;
	}
	
	
	class WrappedMethod{
		private IMethod method;
		
		
		public WrappedMethod(Method method) {
			this.method = wrap(method);
		}
		
		
		public boolean accepts(Object event) {
			return method.accepts(event);
		}
		
		
		public void emit(Object event) throws Exception {
			for(Object object : ClassWrapper.this.objects) {
				emit(object, event);
				
				if(event instanceof ConsumableEvent && ((ConsumableEvent) event).isConsumed()) 
					return;
			}
		}
		public void emit(Object object, Object event) throws Exception {
			Object ret = method.invoke(object, event);
			
			if(event instanceof ConsumableEvent && ret instanceof Boolean && ret.equals(true)) {
				((ConsumableEvent) event).consume();
				
				return;
			}
		}
	}
	
	
	private IMethod wrap(Method method) {
		if(EventUtils.isParameterized(method))
			return new MethodWithGenericParameter(method);
		else
			return new NormalMethod(method, method.getAnnotation(EventListener.class).fieldNames());
	}
}
