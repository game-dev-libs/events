package com.felixcool98.events.fast;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

public class Storage<T> {
	private Map<Integer, LinkedList<T>> map = new HashMap<Integer, LinkedList<T>>();
	
	
	public boolean contains(int hash, T method) {
		if(!map.containsKey(hash))
			return false;
		
		if(!map.get(hash).contains(method))
			return false;
		
		return true;
	}
	
	
	public LinkedList<T> get(int hash){
		if(map.containsKey(hash))
			return map.get(hash);
		
		return new LinkedList<T>();
	}
	
	
	public void add(int hash, T method) {
		if(!map.containsKey(hash))
			map.put(hash, new LinkedList<T>());
		
		map.get(hash).add(method);
	}
	public void remove(T method) {
		for(LinkedList<T> list : map.values()) {
			list.remove(method);
		}
	}
}
