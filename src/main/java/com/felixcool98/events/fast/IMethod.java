package com.felixcool98.events.fast;

public interface IMethod {
	public boolean accepts(Object event);
	public Object invoke(Object object, Object event) throws Exception;
}
