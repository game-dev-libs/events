package com.felixcool98.events.fast;

import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.LinkedList;
import java.util.List;

public class EventUtils {
	public static boolean isParameterized(Method method) {
		return method.getGenericParameterTypes()[0] instanceof ParameterizedType;
	}
	
	public static boolean accepts(Class<?> classs, Method method) {
		if(method.getParameterCount() != 1)
			return false;
		
		if(method.getGenericParameterTypes()[0] instanceof ParameterizedType) {
			ParameterizedType type = (ParameterizedType) method.getGenericParameterTypes()[0];
			
			Class<?> interf = (Class<?>) type.getRawType();
			
			List<Class<?>> genericTypes = new LinkedList<Class<?>>();
			
			for(Type typeArg : type.getActualTypeArguments()) {
				if(typeArg instanceof Class)
					genericTypes.add((Class<?>) typeArg);
				else {
					genericTypes.add((Class<?>) ((ParameterizedType) typeArg).getRawType());
				}
			}
			
			return implementsGenericInterfaceOrExtendsAbstractMethodWithTypes(classs, interf, genericTypes.toArray(new Class[0]));
		}else {
			if(method.getParameterTypes()[0].isAssignableFrom(classs))
				return true;
		}
		
		return false;
	}
	
	
	public static boolean implementsGenericInterfaceOrExtendsAbstractMethodWithTypes(Class<?> classs, Class<?> interf, Class<?>... genericTypes) {
		if(classs.equals(Object.class))
			return false;
		
		if(classs.equals(interf)) {
			if(allowedType(classs, interf, genericTypes))
				return true;
		}
		
		for(Type type : classs.getGenericInterfaces()) {
			if(allowedType(type, interf, genericTypes))
				return true;
		}
		
		Type type = classs.getGenericSuperclass();
		
		if(allowedType(type, interf, genericTypes))
			return true;
		
		return implementsGenericInterfaceOrExtendsAbstractMethodWithTypes((Class<?>) (
				classs.getGenericSuperclass() instanceof ParameterizedType ? 
						((ParameterizedType)classs.getGenericSuperclass()).getRawType() : classs.getGenericSuperclass()),
						interf, genericTypes);
	}
	private static boolean allowedType(Type type, Class<?> interf, Class<?>...genericTypes) {
		if(!(type instanceof ParameterizedType))
			return false;
		
		ParameterizedType paramterized = (ParameterizedType) type;
		
		if(interf.equals(((ParameterizedType) type).getRawType())) {
			for(int i = 0; i < genericTypes.length; i++) {
				if(paramterized.getActualTypeArguments()[i].equals(genericTypes[i]))
					continue;
				else
					return false;
			}
			
			return true;
		}
		
		return false;
	}
}
