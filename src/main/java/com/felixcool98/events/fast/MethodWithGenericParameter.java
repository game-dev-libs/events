package com.felixcool98.events.fast;

import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;

import com.felixcool98.events.GenericEvent;

public class MethodWithGenericParameter implements IMethod {
	private Method method;
	
	
	public MethodWithGenericParameter(Method method) {
		this.method = method;
	}
	
	
	@Override
	public boolean accepts(Object event) {
		if(EventUtils.accepts(event.getClass(), method))
			return true;
		
		if(!(event instanceof GenericEvent)) 
			return false;
		
		if(method.getParameterTypes()[0].isAssignableFrom(event.getClass())) {
			GenericEvent<?> generic = (GenericEvent<?>) event;
			
			if(method.getGenericParameterTypes().length != 1)
				return false;
			
			if(!(method.getGenericParameterTypes()[0] instanceof ParameterizedType))
				return false;
			
			ParameterizedType parameterized = (ParameterizedType) method.getGenericParameterTypes()[0];
			
			if(parameterized.getActualTypeArguments()[0] instanceof ParameterizedType)
				return false;
			else
				return parameterized.getActualTypeArguments()[0].equals(generic.getType());
		}
		
		return false;
	}

	@Override
	public Object invoke(Object object, Object event) throws Exception {
		return method.invoke(object, event);
	}
}
