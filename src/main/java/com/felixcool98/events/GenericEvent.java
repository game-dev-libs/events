package com.felixcool98.events;

public interface GenericEvent<T> {
	public Class<T> getType();
}
