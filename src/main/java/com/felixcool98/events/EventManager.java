package com.felixcool98.events;

import com.felixcool98.events.fast.FastEventManager;

public interface EventManager {
	public static EventManager DefaultEventManager() {
		return new FastEventManager();
	}
	
	
	public void send(Object event);
	public void sendTo(Object listener, Object event);
	
	public void listen(Object listener);
	
	public void stopListening(Object listener);
	
	public void setExceptionHandler(ExceptionHandler handler);
}
